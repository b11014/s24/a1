// S24 Activity:

// >> In the S19 folder, create an activity folder and an index.html and index.js file inside of it.

// >> Link the script.js file to the index.html file.
/*
>> Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)

>> Using Template Literals, print out the value of the getCube variable with a message:
		 The cube of <num> is…*/

		 let getCube = 50 ** 3;
		 	console.log(`The cube of 50 is ${getCube}.`);

/* >> Destructure the given array and print out a message with the full address using Template Literals.
	
		const address = ["258", "Washington Ave NW", "California", "90011"];

			message: I live at <details> */

		const address = ["258", "Washington Ave NW", "California", "90011"]; 
		const [streetNumber, streetName, state, zip] = address;

		console.log(`I live at ${streetName} ${streetName}, ${state} ${zip}.`);

// >> Destructure the given object and print out a message with the details of the animal using Template Literals.
// 		message: <name> was a <species>. He weighed at <weight> with a measurement of <measurement>.	
		const animal = {
			name: "Lolong",
			species: "saltwater crocodile",
			weight: "1075 kgs",
			measurement: "20 ft 3 in"
		};
		
		const{name, species, weight, measurement} = animal;
		console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`);


// >> Loop through the given array of characters using forEach, an arrow function and using the implicit return statement to print out each character
		
		const characters = ['Ironman', 'Black Panther', 'Dr. Strange', 'Shang-Chi', 'Falcon']
		characters.forEach(characters => console.log (`${characters}`)); 

// >> Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
		class Dog{
			constructor(name, age, breed){
				this.name = name
				this.age = age
				this.breed = breed
			}
		};

		let myDog = new Dog();
		myDog.name = "Raki";
		myDog.age = "5";
		myDog.breed = "Japanese Spitz";
		console.log(myDog);

		let myDog2 = new Dog();
		myDog2.name = "Rion";
		myDog2.age = "3";
		myDog2.breed = "Labrador Retriever";
		console.log(myDog2);

// >> Create/instantiate a 2 new object from the class Dog and console log the object